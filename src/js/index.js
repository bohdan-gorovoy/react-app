import React from 'react';
import { render } from 'react-dom';
import Root from 'Root';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createStore } from 'redux';

function people(state = [], action) {
    if (action.type === 'ADD_PERSON') {
        return [
            ...state,
            action.payload
        ];
    }
    //console.log(action);
    return state;
}

const store = createStore(people);

//console.log(store.getState());

store.subscribe(() => {
   console.log('subscribe', store.getState());
});

store.dispatch({
    type: 'ADD_PERSON',
    payload: 'Bohdan Gorovoy'
});

store.dispatch({
    type: 'ADD_PERSON',
    payload: 'Nastya Davydok'
});

render(
    <Router>
        <div>
            <Route path="/" component={Root} />
        </div>
    </Router>,
    document.getElementById('root')
);