import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import LocationCity from 'material-ui/svg-icons/social/location-city';
import { grey300 as iconColor } from 'material-ui/styles/colors';
import axios from 'axios';
import * as API from '../constants/api';

const style = {
    paper: {
        height: 200,
        width: 200,
        margin: 20,
        display: 'inline-block',
        textAlign: 'center'
    },
    icon: {
        width: '80%',
        height: 'auto',
        display: 'inline-block'
    }
};

export default class Form extends Component {
    constructor(props) {
        super(props);
    }

    /*getCurrentWeather(event) {
        event.preventDefault();

        return axios
            .get(API.OPEN_WEATHER_API_URL + '/weather', {
                params: {
                    APPID: API.OPEN_WEATHER_API_KEY,
                    q: this.state.city,
                    units: 'metric'
                }
            })
            .then(response => {
                this.props.updateWeather(response);
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleFormChange(e) {
        this.props.updateForm(e.target);
    }

    handleFormSubmit(e) {
        e.preventDefault();
        //this.props.updateForm(e.firstName.value, e.lastName.value);
        console.log(this.state.firstName);
    }

    handleCityChange(event) {
        this.setState({
            city: event.target.value
        })
    }*/

    render() {
        return(
            <div className="form">
                <Paper style={style.paper} zDepth={3} circle={true}>
                    <LocationCity color={iconColor} style={style.icon}/>
                </Paper>
                <form>
                    <input type="text" name="cityName" placeholder="Type city name"/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    };
}
