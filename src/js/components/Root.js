import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Form from 'Form';
//import History from 'History';
import { Link, NavLink } from 'react-router-dom';

export default class Root extends Component {

    constructor(props) {
        super(props);

        /*const defaultValues = {
            firstName: 'First Name',
            lastName: 'Last Name',
            weather: ''
        };

        this.defaultState = {
            firstName: this.props.firstName || defaultValues.firstName,
            lastName: this.props.lastName || defaultValues.lastName,
            weather: defaultValues.weather
        };

        this.state = this.defaultState;*/

    }

    /*handleFormUpdate(formData) {
        if (formData.name === 'firstName') {
            this.setState({
                firstName: formData.value
            });
        } else {
            this.setState({
                lastName: formData.value
            })
        }
        console.log(formData.value);
    }

    handleWeatherUpdate(response) {
        console.log(response.data.main);
        this.setState({
            weather: response.data.main.temp
        });
    }*/

    render() {
        return(
            <div className="root">
                <MuiThemeProvider>
                    <Form />
                    {/*<History />*/}
                    {/*<p>{this.state.firstName}</p>
                    <p>{this.state.lastName}</p>
                    <p>Current weather: {this.state.weather}</p>
                    <Form updateForm={::this.handleFormUpdate} updateWeather={::this.handleWeatherUpdate} />
                    <Link to={'/user/bohdan'}>Get user name</Link>
                    <br/>
                    <Link to="/weather">Get weather</Link>
                    <NavLink to="/home">Home</NavLink>
                    <NavLink to="/Contacts">Contacts</NavLink>*/}
                </MuiThemeProvider>
            </div>
        )
    }
}