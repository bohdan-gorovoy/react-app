'use strict';
const Webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

const compiler = Webpack(config);

const server = new WebpackDevServer(compiler, {
    contentBase: 'public',
    publicPath: config.output.publicPath
});

server.listen(8080, 'localhost', (err, res) => {
   if (err) return console.log(err);
    console.log('Listening on 8080 port');
});