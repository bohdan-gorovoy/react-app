
const path = require('path');

module.exports = {
    entry: [
        './src/js/index.js'
    ],
    output: {
        path: path.join(__dirname, 'public/assets/js'),
        filename: 'bundle.js',
        publicPath: '/assets/js'
    },
    resolve: {
        alias: {
            Root: path.join(__dirname, 'src/js/components', 'Root.js'),
            Form: path.join(__dirname, 'src/js/components', 'Form.js'),
            History: path.join(__dirname, 'src/js/components', 'History.js'),
        },
        extensions: ['.js', '.jsx']

    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: [
                    /(node_modules)/
                ],
                loaders: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'stage-0']
                        }
                    }
                ]
            }
        ]
    }
};